FROM klakegg/hugo:0.83.1 as build
COPY . /src
RUN hugo --source=/src/ --destination=/public/

FROM nginx:stable-alpine
COPY --from=build /public/ /usr/share/nginx/html/
EXPOSE 80

