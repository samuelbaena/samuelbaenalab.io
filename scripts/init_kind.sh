#!/bin/bash
#
# Usage: bash scripts/init_kind.sh

kind delete cluster || true

docker build -f Dockerfile -t samuelbaena:0.1.0 .

kind create cluster --config=scripts/kind_cluster.yml
kind load docker-image samuelbaena:0.1.0

VERSION=$(curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/stable.txt)
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/${VERSION}/deploy/static/provider/kind/deploy.yaml
